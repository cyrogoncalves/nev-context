# NEV Context

Núcleo de Estudo da Violência.

### Banco de dados

Para transferir a mudança dos modelos para a migration, rode `./nev-context/manage.py makemigrations`

Para alterar o banco de dados a partir das migrations, rode `./nev-context/manage.py migrate`

Credenciais de acesso:

Usuário: nev
Senha:   nev

### Dependências

Para instalar as dependências, rode `pip3 install -r requirements.txt`