from rest_framework import viewsets
from hackaton.nev.models import NevUser
from hackaton.nev.serializers import NevUserSerializer


class NevUserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = NevUser.objects.all().order_by('-name')
    serializer_class = NevUserSerializer
