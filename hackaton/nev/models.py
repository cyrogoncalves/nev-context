from __future__ import unicode_literals

from django.db import models


class Region(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = 'region'


class University(models.Model):
    name = models.CharField(max_length=255)
    badge_path = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = 'university'


class Event(models.Model):
    name = models.CharField(max_length=255)
    date = models.DateField

    class Meta:
        db_table = 'event'


class Article(models.Model):
    name = models.CharField(max_length=255)
    date = models.DateField
    events = models.ManyToManyField(Event, through='ArticleEvent')
    file_path = models.CharField(max_length=255, null=True)
    mime_type = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = 'article'


class ArticleEvent(models.Model):
    id_article = models.ForeignKey(Article)
    id_event = models.ForeignKey(Event)


class ArticleText(models.Model):
    id_article = models.ForeignKey(Article)
    article_text = models.TextField

    class Meta:
        db_table = 'article_text'


class EventText(models.Model):
    id_event = models.ForeignKey(Event)
    event_text = models.TextField()

    class Meta:
        db_table = 'event_text'


class NevUser(models.Model):
    id_university = models.ForeignKey(University, null=True)
    id_region = models.ForeignKey(Region, null=True)
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    gender = models.CharField(max_length=1, null=True)
    age = models.IntegerField(null=True)
    token = models.CharField(max_length=255)

    class Meta:
        db_table = 'nev_user'


class Poll(models.Model):
    id_user = models.ForeignKey(NevUser)
    id_article = models.ForeignKey(Article)
    name = models.CharField(max_length=255)
    question = models.CharField(max_length=255)
    creation_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'poll'


class Submission(models.Model):
    id_user = models.ForeignKey(NevUser)
    creation_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'submission'


class PollAnswer(models.Model):
    id_submission = models.ForeignKey(Submission)
    answer_text = models.TextField

    class Meta:
        db_table = 'poll_answer'


class PeerReview(models.Model):
    id_article_event = models.ForeignKey(ArticleEvent, null=True)
    id_poll = models.ForeignKey(Poll, null=True)
    id_nev_user = models.ForeignKey(NevUser)
    positive_reaction = models.BooleanField(default=False)

    class Meta:
        db_table = 'peer_review'

