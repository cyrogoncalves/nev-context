from hackaton.nev.models import NevUser
from rest_framework import serializers


class NevUserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = NevUser
        fields = ('name', 'email', 'gender', 'age', 'id_university', 'id_region')